# Test Project for OpenJDK Early Access Docker Image with Maven
![Build Status](http://gitlab.com/codenomads/openjdk-project-amber-raw-string-literals/badges/master/build.svg)

## Running Locally

After checking out the code you can either build the JDK 11 Raw String Literals branch locally by following:
```bash
hg clone http://hg.openjdk.java.net/amber/amber
cd amber
hg update raw-string-literal
sh configure && make images
./build/*/images/jdk/bin/java -version"
```

The you can configure your IDE to point to the new JDK. Or you can use the Docker image for the GitLab CI/CD pipelines.

```bash
 docker run -v $(pwd):/home/javauser --rm codenomads/openjdk-project-amber-raw-string-literals bash build-pipeline.sh
```

## Issues

Feedback or issues are most welcome, please use the issues link [here](https://gitlab.com/ozoli/openjdk-project-amber-raw-string-literals/issues).
