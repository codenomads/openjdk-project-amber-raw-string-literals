#!/usr/bin/env bash

MAVEN_CLI_OPTS="--batch-mode --errors --fail-at-end --show-version"

echo "Compiling Source..."
mvn clean compile $MAVEN_CLI_OPTS

echo "Compiling Test Source..."
mvn test-compile $MAVEN_CLI_OPTS

echo "Running unit tests..."
mvn test
