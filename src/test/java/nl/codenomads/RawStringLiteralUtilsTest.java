package nl.codenomads;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for {@link RawStringLiteralUtils}
 */
public class RawStringLiteralUtilsTest {
    @Test
    public void testSqlSnippet() {
        assertEquals("Expected: select * from USERS where id=1",
                "select * from USERS where id=1",
                StringUtils.normalizeSpace(RawStringLiteralUtils.sqlSnippet()));
    }

    @Test
    public void testHTMLSnippet() {
        assertEquals("Expected: <html> <body> <p>Hello World.</p> </body> </html>",
                "<html> <body> <p>Hello World.</p> </body> </html>",
                StringUtils.normalizeSpace(RawStringLiteralUtils.htmlSnippet()));
    }
}
