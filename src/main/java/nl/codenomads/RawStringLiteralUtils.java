package nl.codenomads;

/**
 * Utility class to show example of OpenJDK Project Amber language features, JDK 11 or 12.
 */
public class RawStringLiteralUtils {
    /**
     * SQL for selecting the first user in the USER table
     * @return the SQL as a {@String} without spaces.
     */
    public static String sqlSnippet() {
        final var sql =
        ```select *  from USERS 
        where id=1```;
        return sql;
    }

    /**
     * HTML Hello World snippet
     * @return the HTML as a {@String} without spaces.
     */
    public static String htmlSnippet() {
        return `<html>
                   <body>
                       <p>Hello World.</p>
                   </body>
               </html>`;
    }
}
